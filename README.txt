
Expose User Email 1.x for Drupal 7.x
-----------------------
Expose User Email exposes the user email as a field on the account Manage 
Display tab (admin/config/people/accounts/display).

The email is exposed only to users that were granted the permission "View 
user email addresses".
 

Installation
------------
Expose User Email can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it on the `admin/build/modules`
page.


API
---
This module implements the theme `user_email` which modules may overwrite to
change the way the email is displayed.

Maintainers
-----------
- mariancalinro (Calin Marian)

